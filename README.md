# Momo Store aka Пельменная №2
[Momo Store repository](https://gitlab.praktikum-services.ru/02_idrisov_r/momo_store)

[Momo Store Infrastructure repository](https://gitlab.praktikum-services.ru/02_idrisov_r/momo-store-infrastructure)

- Prometheus
- Loki + promtail
- Alertmanager
- Grafana


[Momo Store site](https://diploma-idrisov.task8-2.tk)

[Prometheus site](https://prometheus.task8-2.tk)

[Alertmanager site](https://alertmanager.task8-2.tk)

[Grafana site](https://grafana.task8-2.tk)

login: ********

Diploma presentation: diploma-idrisov.pdf

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Frontend

```bash
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

## Backend

```bash
go run ./cmd/api
go test -v ./... 
```
